<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'Annuaire' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'mehdi' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'mehdi' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'sxk}+nYtl=3=zrN}2}H@H8x!{{i*[~,LCO:gS=M4^Tg`V<JtN4b~!=kPk.i^5I8B' );
define( 'SECURE_AUTH_KEY',  'hBf~0%9NZ<i)lC:iRihdH)aw?4cl[&VfXEMrzgUZqD_}dy)sjnK<Y8:p8~+TT3bA' );
define( 'LOGGED_IN_KEY',    '2.?hq2i|vAr{J&C43seW<G;K.&zM#aw4>RVVty[5{ri:nOfJD[6>5)^?z~wUrPkc' );
define( 'NONCE_KEY',        'd5l>Md>os9$TrL[[K~gQuq%{=sIMPKJ*%<`-WfN fyO!51]])I[[uj)>S^>z=Qij' );
define( 'AUTH_SALT',        '0-FF.+Tp6)Bb6d[[<5*eh*sRlCKmpLPF3d.ybXTt-_OzgZd;1hpoYDp#80D=&%Dr' );
define( 'SECURE_AUTH_SALT', ',W2r/0f]/f|k}tlQ4zeri$T`%*5mpaoo5%:[Wejv{v#IZU!k 9F:2MCOd5RG!pU:' );
define( 'LOGGED_IN_SALT',   'Y@4eIJLuf48;k/sjLzR[<PY0<zX~=Jv(1Vx<?u1}3iRk|L9<Z~}aE570,({!V:@}' );
define( 'NONCE_SALT',       '}@#vuh:r5^Q[?B6M/&M*!4uj:_bgsX4/4>6`n/e{4caQq70nI9k:KLuuqhqp6V2;' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
